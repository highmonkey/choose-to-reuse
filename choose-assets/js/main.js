
$(document).ready(function() {
	var service;
	var cat;
	var range;
	var zip;
	var console = (window.console = window.console || {});

		//CONTACT BAR FUNCTION//
	function initContactBar () {
		// Toggle contact drawer open/close
		$('.contact-toggle').bind('click', function() {
			$('.cp_contact-block').slideDown("slow");
			//Brings user back to the top of the page
			$('html, body').animate({ scrollTop: 0 }, 'slow');
		});
		$('.icon-x-close').bind('click', function() {
			$('.cp_contact-block').slideUp("fast");
		});
	}

	//MENU BAR FUNCTION//
	function initMenuBar () {
		// Index hero img selection boxes
		$('.menu-selection').bind('mouseenter click', function () {
			$(this).addClass('active').siblings().removeClass('active');
		});

		// Active state on desktop
		$('.menu-bar a').bind('click' , function (e){
			e.preventDefault();
			var secondaryTabSection = $(this).parents('.menu-main');
			var tabs = $(this).siblings('menu-selection');

			tabs.removeClass('active');
			$(this).addClass('active');

			// Carries the current active state in between desktop and mobile display
			if (secondaryTabSection.length) {
				var mobileTabs = secondaryTabSection.find('.menu-bar-mobile li a');
				var currentText = secondaryTabSection.find('.dropdown-toggle div:first-child');
				var i = $(this).index();
				mobileTabs.removeClass('active');
				mobileTabs.parent('li').eq(i).children('a').addClass('active');
				var newText = secondaryTabSection.find('.menu-bar-mobile li a.active').text();
				currentText.text(newText);
			}
		});

		// Active state on mobile
		$('.menu-bar-mobile a').bind('click' , function (e){
			var secondaryTabSection = $(this).parents('.menu-main');
			var currentText = $(this).parents('.menu-bar-mobile').find('.dropdown-toggle div:first-child');
			var newText = $(this).text();

			$('.menu-bar-mobile a').removeClass('active');
			$(this).addClass('active');
			currentText.text(newText);

			// Carries the current active state in between desktop and mobile display
			if (secondaryTabSection.length) {
				var desktopTabs = secondaryTabSection.find('.menu-bar a');
				var i = $(this).parent().index();
				desktopTabs.removeClass('active');
				desktopTabs.eq(i).addClass('active');
			}
		});

		// Toggle caret up/down and open dropdown menu in the category
		$('.menu-bar-mobile').bind('click' , function (e){
			e.preventDefault();
			$(this).find('.toggle').toggleClass('icon-caret-up icon-caret-down');
			$(this).find('.dropdown-menu').toggleClass('open close');
		});

		// Toggle caret up/down and update dropdown with selected text
		$('.location-form a').bind('click' , function (e){
			e.preventDefault();
			var currentText = $('.location-form').find('.dropdown-toggle span:first-child');
			var newText = $(this).text();

			currentText.text(newText);
		});

		// Toggle caret up/down and update dropdown with selected text
		$('.location-form').bind('click' , function (e){
			e.preventDefault();
			$(this).parent().find('.toggle').toggleClass('dropdown-caret-up dropdown-caret-down');
		});

		// Toggle caret up/down and text drawer open/close
		$('.collapse').on('hide.bs.collapse show.bs.collapse', function() {
			$(this).parent().find('.collapse').toggleClass('collapse-up collapse-down');
			$(this).parent().find('.caret').toggleClass('caret-up caret-down');
		});

		//Activates the uniform on the select dropdown menu
		$(function(){ $("select").uniform(); });

	}//end initMenuBar

	//BOTTOM BAR FUNCTION//
	function initBottomBar () {
		// Toggle caret up/down and the text bottom drawer open/close
		$('h5').bind('click', function() {
			$(this).toggleClass('open');
			$(this).parents().find('.collapse').toggleClass('collapse-up');
		});
	}

	//LIST BUSINESS FORM VALIDATION FUNCTION//
	function initListBusiness () {
		var checkInputVar = 0;
		var regExPhone = 0;
		var regExEmail = 0;
		var checkCheckVar = 0;
		var total = 0;
		var isFormValid = false;

		if ( $('form').hasClass('idea-event') ){
			checkCheckVar = 1;
		}
		else {
			checkCheckVar = 0;
		}

		function errorBanner () {
			total = regExPhone + regExEmail + checkInputVar + checkCheckVar;
			if (total != 4) {
				$('.error-form').css('display' , 'inline-block');
			}
			else {
				$('.error-form').css('display' , 'none');
				isFormValid = true;
			}
		}

		function RegExPhone (el) {
			var that = el;
			var str = $(el).val();
			var patt = new RegExp("^[0-9 -().]");
			var res = patt.test(str);
			if (!res || $(that).val() == '') {
				$(that).addClass('error');
			}
			else {
				$(that).removeClass('error');
				regExPhone = 1;
			}
		}

		function RegExEmail (el) {
			var that = el;
			var str = $(el).val();
			var patt = new RegExp("^[A-z0-9._%+-]+@[A-z0-9.-]+.[A-z]{2,6}$");
			var res = patt.test(str);
			if (!res || $(that).val() == '') {
				$(that).addClass('error');
			}
			else {
				$(that).removeClass('error');
				regExEmail = 1;
			}
		}

		function checkInput (el) {
			var that = el;
			//CHECK FOR INPUT TEXT VALUES
			if ( $(that).val() == '') {
				$(that).addClass('error');
			}
			else {
				$(that).removeClass('error');
				checkInputVar = 1;
			}
		}

		function checkInputText (el) {
			var that = el;
			//CHECK TO SEE IF INPUT IS EMAIL OR PHONE NUMBER OR PLAIN TEXT
			if ($(that).attr('name') == "phonenumber") {
				RegExPhone(that);
			}
			else if ($(that).attr('name') == "email") {
				RegExEmail(that);
			}
			else {
				checkInput(that);
			}
		}

		function checkCheckboxTotal () {
			if ( $('.service').is(':checked') && $('.category').is(':checked') && $('.googleid').is(':checked') ) {
				checkCheckVar = 1;
			}
		}

		function checkCheckbox (el) {
			var that = el;
			//CHECK FOR CHECKBOX VALUES
			if ( $(that).find('input').is(':checked') ) {
				$(that).removeClass('error');
				checkCheckboxTotal();
			}
			else {
				$(that).addClass('error');
			}
		}

		function checkGoogleText (el) {
			var that = el;
			//CHECK FOR GOOGLE ID TEXT BOX VALUE
			$(that).removeClass('error');
			if ( $('.google_id_text').val() == '') {
				$('.google_id_text').addClass('error');
				checkCheckVar = 0;
			}
			else {
				$('.google_id_text').removeClass('error');
				checkCheckboxTotal();
			}
		}

		function checkFieldSet (el) {
			var that = el;
			//CHECK FOR CHECKBOX VALUES
			if ( $(that).is('.google-id') && $('.googleid_radio').is(':checked') ) {
				checkGoogleText(that);
			}
			else {
				checkCheckbox(that);
			}
		}

		function checkLog () {
			// CHECK FOR EACH REQUIRED FIELD //
			$('.required').each(function() {
				if ( $(this).is('input:text') ) {
					checkInputText(this);
				}
				else if ( $(this).is('textarea') ) {
					checkInput(this);
				}
				else if ( $(this).is('fieldset') ) {
					checkFieldSet(this);
				}
			});
			errorBanner(total);
		}

		function reset() {
			checkInputVar = 0;
			regExPhone = 0;
			regExEmail = 0;
			checkCheckVar = 0;
			total = 0;
		}

		$('form').submit( function () {
			checkLog();
			if (isFormValid == true) {
				reset();
				e.preventDefault();
				return true;
			}
			else {
				return false;
			}
		});
	}

	//CIRCLE CLICK FUNCTION//
	function initCircle () {
		// Toggle the individual text to each circle on and off
		$('.circle').bind('click', function() {
			$(this).siblings('.information-display').toggleClass('display');
		});
	}

	//CIRCLE CLICK FUNCTION//
	function initGoogleToggle () {
		// Toggles the Google ID text box upon click of the
		// linked radio button
		$('input.googleid_radio').bind('click', function() {
			$('.google-id-display').toggleClass('display');
			$('.google_id_text').addClass('error');
		});
		$('input.nongoogleid_radio').bind('click', function() {
			$('.google-id-display').removeClass('display');
		});
	}

	//STICKY FOOTER//
	function getBodyHeight() {
		return $("body").height();
	}
	function getWindowHeight() {
		return $(window).height();
	}
	function getFooterHeight() {
		return $('.cp_footer').outerHeight(true);
	}
	function positionFooter() {
		getFooterHeight();
		if (getWindowHeight() > getBodyHeight()) {
			$(".cp_footer").css("position","absolute").css("bottom",0);
		}
		if (getWindowHeight() < ( getBodyHeight() + getFooterHeight())) {
			$(".cp_footer").css("position","static");
		}
	}
	//MAP SEARCH//
	var getSearchValues = function () {
		cat = $("select[name='categories']:visible option:selected").val();
		range = 50;
		zip = $("input[name='zip']").val();
		if ( $('.visible-xs').is(":visible") ) {
			mobilevalues();
			return true;
		}
		else {
			desktopValues();
		}
	};
	var desktopValues = function () {
		service = $("div.services .active").attr('data-value');
	};
	var mobilevalues = function () {
		service = $("ul.services .active").attr('data-value');
	};



	//////////////////
	//FUNCTION CALLS//

	//CALLS THE FUNCTION TO TOGGLE THE CONTACT INFORMATION//
	if ($('.cp_contact-block')) {
		initContactBar();
	}

	//CALLS THE FUNCTION, IF MENU-SELECTION (SELL/DONATE/BUY..ETC) HAS LENGTH//
	if ($('.menu-selection') || $('.cp_map-search-block')) {
		initMenuBar();
	}

	//CALLS THE FUNCTION FOR TOGGLING THE BOTTOM BAR ON THE HOME PAGE//
	if ($('.bottom-bar')) {
		initBottomBar();
	}

	//CALLS THE FUNCTION TO FORM VALIDATION//
	if ($('.cp_list-business')) {
		initListBusiness();
		initCircle();
		initGoogleToggle();
	}

	//CALLS THE FUNCTION FOR STICKY FOOTER//
	positionFooter();
	$(window).on('resize', function() {
		positionFooter();
	});


	//CALLS TO BUILD QUERY STRING AND GO TO MAP//
	if ($('#baseSearch')) {
		$('button#baseSearch').bind('click' , function (e) {
			e.preventDefault();
			getSearchValues();
			var query = "service=" + service + "&category=" + cat + "&zip=" + zip;
			var url = "/map?"+query;
			window.location = url;
		});
	}
});


