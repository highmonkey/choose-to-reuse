$(function() {
    var map;
    var markersArray = [];
    var infoWindows = [];
    var mapLat = 45.0209;
    var mapLong = -93.5095;
    var service;
    var cat;
    var range = 50;
    var zip = 55429;
    var qService;
    var qCat;
    var qZip = 55429;

    var generalUtils = {
        bindSearchBtn : function () {
            $('.search-btn').bind('click' , mapUtils.getLocations);
        },
        getSearchValues : function () {
            if ( $('.visible-xs').is(":visible") ) {
                this.mobilevalues();
            }
            else {
                this.desktopValues();
            }
        },
        showTips : function (service) {
            var tips = $('.cp_tip-notes-block');

            tips.find('article').removeClass('active');
            tips.find('.' + service).addClass('active');
        },
        desktopValues : function () {
            service = $("select[name='services'] option:selected").val();
            cat = $("select[name='d-categories']:visible option:selected").val();
            zip = $("input[name='d-zip']").val() || mapLat+','+mapLong;
        },
        mobilevalues : function () {
            service = $("ul.services .active").attr('data-value');
            cat = $("select[name='m-categories']:visible option:selected").val();
            zip = $("input[name='m-zip']").val() || mapLat+','+mapLong;
        },
        drawerToggle : function(target) {
            $(target).addClass('active').siblings().removeClass('active');
        },
        scrollTo : function (i) {
            var container = $('.cp_map-sidebar-results > section'),
                target = $('.map-result').eq(i);
                this.drawerToggle(target);

            container.animate({
                scrollTop: target.offset().top - container.offset().top + container.scrollTop()
            });
        },
        cleanMapSideBar : function () {
            $('.hours-list li').each(function(){
                var str = $(this).text(),
                    i = str.indexOf(':'),
                    l = str.length,
                    same = str.substring(0 , i),
                    res = str.substring(i+1, l);

                $(this).html('<span class="twelve semibold"><span class="hour-col">'+same +':</span>'+res+'</span>');
            });
        },
        dropDownHandlers : function() {
            var selects = $('.category select');
            var mobileServiceGroup = $('.menu-bar-mobile');
            var moblieServiceText = mobileServiceGroup.find('[data-bind="label"]');
            var mobileService = $('.services a');
            var desktopService = $('.type select');

            mobileService.on('click' , function(){
                var val = $(this).attr('data-value');

                desktopService.find('option[value="' + val + '"]').prop('selected', true);
                $.uniform.update();
            });
            desktopService.on('change' , function(){
                var val = $(this).val();
                var target = $('[data-value="' + val + '"]');
                var text = target.text();

                $(mobileServiceGroup).find('.active').removeClass('active');
                moblieServiceText.text(text);
                target.addClass('active');
            });
            selects.on('change' , function(){
                var val = $(this).val();

                selects.find('option[value="' + val + '"]').prop('selected', true);
                $.uniform.update();
            });

        },
        listResultHandlers : {
            unclassifiedSectionToggle: function () {
                $(this).toggleClass('open');
                $(this).next('.drawer').toggleClass('collapse-up');
            },
            unclassifiedItemToggle: function () {
                var item = $(this).parents('.list-result')
                item.toggleClass('open');
                item.children('.drawer').toggleClass('collapse-up');
            },
            init: function () {
                $('.list-result .collapse-toggle').on('click', this.unclassifiedItemToggle);
                $('.cp_map-unclassified-results').slideDown();
                $('.results-drawer').bind('click', this.unclassifiedSectionToggle);
            }
        }
    };
    var mapUtils = {
        drawMap : function () {
            var isDraggable = $(document).width() > 480 ? true : false; // If document (your website) is wider than 480px, isDraggable = true, else isDraggable = false
            var myOptions = {
                zoom : 8,
                maxZoom: 17,
                panControl: false,
                streetViewControl: false,
                scrollwheel: false,
                draggable: isDraggable,
                center: new google.maps.LatLng(mapLat, mapLong),
                mapTypeId : google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map"), myOptions);
        },
        getLocations : function () {
            $.ajax({
                url: 'http://scdev/sitecore/content/Choose to Reuse/Configuration/DataEndpoint?',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data) {
					console.log("json data loaded");
                    mapUtils.buildMap(data.contacts);
                },
				error: function(data, txtStatus, errorThrown) {
					console.log("error occured");
					console.log(txtStatus);
					console.log(errorThrown);
					console.dir(data);
                }
            });
        },
        getLocation : function (zip) {
            var geocoder = new google.maps.Geocoder();
            var address = zip;
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    mapLat = results[0].geometry.location.lat();
                    mapLong = results[0].geometry.location.lng();
                } else {
                    alert("Request failed.")
                }
            });
        },
        distance : function (lat1, lon1, lat2, lon2, unit) {
            var radlat1 = Math.PI * lat1/180;
            var radlat2 = Math.PI * lat2/180;
            var theta = lon1-lon2;
            var radtheta = Math.PI * theta/180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            dist = Math.acos(dist);
            dist = dist * 180/Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit=="K") { dist = dist * 1.609344 }
            if (unit=="N") { dist = dist * 0.8684 }
            return dist
        },
       closeAllInfoWindows : function () {
            for (var i=0;i<infoWindows.length;i++) {
                infoWindows[i].close();
            }
        },
        clearMarkers : function () {
            for (var i = 0; i < markersArray.length; i++) {
                markersArray[i].setMap(null);
            }
            markersArray = [];
        },
        openResultWindow : function (el) {
            var i = $(el).index();
            google.maps.event.trigger(markersArray[i], 'click');
        },
        buildMarkers : function (map, name , phone,  lat, lng , i ) {
            var circle = new google.maps.Circle({
                path: google.maps.SymbolPath.CIRCLE,
                fillOpacity: 1,
                fillColor: '#f8963c',
                strokeOpacity: 0,
                strokeColor: '#fff000',
                scale: 10 //pixels
            }),
            marker = new google.maps.Marker({
                position: new google.maps.LatLng( lat, lng),
                map: map,
                tel: phone,
                web:  i,
                icon: circle,
                title: name,
                visible: true
            });
            var content= "<div style='background-color:transparent;padding:5px;'><span class='twelve'>"+name+"</span></div>";
            var infowindow = new google.maps.InfoWindow();

            (function (marker, content , i) {
                google.maps.event.addListener(marker, "click", function (e) {
                    mapUtils.closeAllInfoWindows();
                    //marker.setIcon(blueMarker);
                    infowindow.setContent(content);
                    infowindow.open(map, marker , generalUtils.scrollTo(i));
                    var center = new google.maps.LatLng(lat, lng);
                    map.panTo(marker.getPosition());

                });
            })(marker, content , i);

            infoWindows.push(infowindow);
            markersArray.push(marker);
        },
        buildMap : function (data) {
		console.log("in build map");
            var i = 0;
            var mapResults = [];
            var listResults = [];
			var counter = 0;
			
            generalUtils.getSearchValues();
            mapUtils.getLocation(zip);
            generalUtils.showTips(service);

            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    var obj = data[key];
                    var resultCats = obj.categories;
                    var showOnMap = obj.displayOnMap;
                    var searchedCats = cat;
					
					if(obj.lat == 0 || obj.lng == 0){
						//mapUtils.getLocation(obj.zipCode);
						//console.log("getting lat/lng of: " + obj.formattedAddress);
					}

                    var locationDist = mapUtils.distance(mapLat,mapLong, obj.lat , obj.lng);

					//Debug:
					if(locationDist < 50){
						//console.log("locationDist: " + locationDist);
						//console.log("searchedCats: " + searchedCats);
					}
                    if (obj[service] && resultCats.indexOf(searchedCats) > -1) {
					
						if(locationDist < range){
							if (showOnMap) {
								console.log("Adding object to map");
								mapResults.push(obj);
								console.dir(obj);
							} else {
							console.log("Adding object to list");
								listResults.push(obj);
								console.dir(obj);
							}
						}
					}
                }
				counter++;
            }
			
			
            updateUi.drawMapResults(mapResults);
            updateUi.drawListResults(listResults);
            var center = new google.maps.LatLng(mapLat, mapLong);
            map.setZoom(8);
            map.panTo(center);
        },
        checkForQuery : function () {
            // Read a page's GET URL variables and return them as an associative array.
            var vars = [],
                hash,
                hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

            var setQuery = function (qService, qCat, qZip) {
                setTimeout(function () {
                    //MOBILE
                    $("input[name='m-zip']").val(qZip);
                    var txtServM = $('ul.services li a[data-value="' + qService + '"]').text();
                    $('button div.semibold[data-bind="label"]').text(txtServM);
                    $('.location-dropdown.m-category div.selector span').text(qCat);

                    //DESKTOP
                    $("input[name='d-zip']").val(qZip);
                    $('option[value="' + qService + '"]').prop('selected', true);

                    $('option[value="' + qCat + '"]').prop('selected', true);

                    $.uniform.update();
                    mapUtils.getLocations();
                },500)
            };
            var initQuery = function () {
                for(var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars.push(hash[1]);
                }

                qService = vars[0];
                qCat = vars[1].replace(/%20/g, " ");
                qZip = vars[2] || zip;

                setQuery(qService, qCat, qZip);
            };

            if (window.location.href.indexOf('?') < 0) {
                return;
            } else {
                initQuery();
            }

        }

    };
    var updateUi = {
        drawListResults : function (filtered) {
            if (filtered.length > 0 ) {
                var htmlContainer = $('.list-results');
                var templateString = $('#listTemplate').html();

                this.drawListUi(htmlContainer , templateString , filtered);
            }

        },
        drawMapResults : function (filtered) {
            var htmlContainer = $('.map-results');
            var templateString = $('#locationTemplate').html();
			var alternateTemplateString = $('#locationTemplateNoPlacesID').html();
            var resultsCount = $('.results-count span');

            this.drawMapUi(htmlContainer , templateString , filtered);
            resultsCount.text(filtered.length).parents('.results-count').animate({
                opacity: '1.0'
            });
        },
        drawMapUi : function (htmlContainer , templateString , filtered) {
            var i = 0;
            var putOnPage = function (place , obj, hasPlacesID) {
			
			if(hasPlacesID){
                var thisTemplateString = templateString.slice('');
                thisTemplateString = thisTemplateString.replace('{{result}}', i + 1);
                thisTemplateString = thisTemplateString.replace('{{name}}', place.name);
                thisTemplateString = thisTemplateString.replace('{{googleMap}}', place.url);
                thisTemplateString = thisTemplateString.replace('{{Address}}', place.formatted_address);
                thisTemplateString = thisTemplateString.replace('{{Phone}}', place.formatted_phone_number);
				
				thisTemplateString = thisTemplateString.replace('{{hoursM}}', place.opening_hours.weekday_text[0]);
				thisTemplateString = thisTemplateString.replace('{{hoursT}}', place.opening_hours.weekday_text[1]);
				thisTemplateString = thisTemplateString.replace('{{hoursW}}', place.opening_hours.weekday_text[2]);
				thisTemplateString = thisTemplateString.replace('{{hoursTH}}', place.opening_hours.weekday_text[3]);
				thisTemplateString = thisTemplateString.replace('{{hoursF}}', place.opening_hours.weekday_text[4]);
				thisTemplateString = thisTemplateString.replace('{{hoursS}}', place.opening_hours.weekday_text[5]);
				thisTemplateString = thisTemplateString.replace('{{hoursSU}}', place.opening_hours.weekday_text[6]);
				
				thisTemplateString = thisTemplateString.replace(/{{Website}}/g,place.website);
				thisTemplateString = thisTemplateString.replace('{{information}}', obj.information);
				
                if (obj.sellsUsed) {
                    thisTemplateString = thisTemplateString.replace('{{sellsUsed}}', 'show');
                }
                if (obj.repairs) {
                    thisTemplateString = thisTemplateString.replace('{{repairs}}', 'show');
                }
                if (obj.buysUsed) {
                    thisTemplateString = thisTemplateString.replace('{{buysUsed}}', 'show');
                }
                if (obj.dropOff) {
                    thisTemplateString = thisTemplateString.replace('{{dropOff}}', 'show');
                }
                if (obj.webOnly) {
                    thisTemplateString = thisTemplateString.replace('{{webOnly}}', 'show');
                }
                if (obj.pickup) {
                    thisTemplateString = thisTemplateString.replace('{{pickup}}', 'show');
                }
                if (obj.rents) {
                    thisTemplateString = thisTemplateString.replace('{{rents}}', 'show');
                }
                if (obj.other) {
                    thisTemplateString = thisTemplateString.replace('{{other}}', 'show');
                }

                //map, name , phone,  lat, lng, i
				
					mapUtils.buildMarkers(map, place.name , place.formatted_phone_number,  place.geometry.location.lat(), place.geometry.location.lng() , i );
				}
				else{
				console.log("Business doesn't have a places ID");
				var thisTemplateString = templateString.slice('');
                thisTemplateString = thisTemplateString.replace('{{result}}', i + 1);
                thisTemplateString = thisTemplateString.replace('{{name}}', place.name);
                thisTemplateString = thisTemplateString.replace('{{googleMap}}', place.url);
                thisTemplateString = thisTemplateString.replace('{{Address}}', place.formattedAddress);
                thisTemplateString = thisTemplateString.replace('{{Phone}}', place.phone);
				thisTemplateString = thisTemplateString.replace('{{hoursM}}', "");
				thisTemplateString = thisTemplateString.replace('{{hoursT}}', "");
				thisTemplateString = thisTemplateString.replace('{{hoursW}}', "");
				thisTemplateString = thisTemplateString.replace('{{hoursTH}}', "");
				thisTemplateString = thisTemplateString.replace('{{hoursF}}', "");
				thisTemplateString = thisTemplateString.replace('{{hoursS}}', "");
				thisTemplateString = thisTemplateString.replace('{{hoursSU}}', "");
				thisTemplateString = thisTemplateString.replace(/{{Website}}/g,place.website);
				thisTemplateString = thisTemplateString.replace('{{information}}', obj.information);
				
                if (obj.sellsUsed) {
                    thisTemplateString = thisTemplateString.replace('{{sellsUsed}}', 'show');
                }
                if (obj.repairs) {
                    thisTemplateString = thisTemplateString.replace('{{repairs}}', 'show');
                }
                if (obj.buysUsed) {
                    thisTemplateString = thisTemplateString.replace('{{buysUsed}}', 'show');
                }
                if (obj.dropOff) {
                    thisTemplateString = thisTemplateString.replace('{{dropOff}}', 'show');
                }
                if (obj.webOnly) {
                    thisTemplateString = thisTemplateString.replace('{{webOnly}}', 'show');
                }
                if (obj.pickup) {
                    thisTemplateString = thisTemplateString.replace('{{pickup}}', 'show');
                }
                if (obj.rents) {
                    thisTemplateString = thisTemplateString.replace('{{rents}}', 'show');
                }
                if (obj.other) {
                    thisTemplateString = thisTemplateString.replace('{{other}}', 'show');
                }
				
				
				
					mapUtils.buildMarkers(map, obj.name , obj.formatted_phone_number,  obj.lat, obj.lng , i );
				}
                $(htmlContainer).append(thisTemplateString);
            };
			
            mapUtils.clearMarkers();
            htmlContainer.children().remove();

            for (var key in filtered) {
                var obj = filtered[key];
				//Only try to get places information if JSON object has a non empty places id
				if(obj.placesId != ""){
				console.log("**************placesId not empty*********************");
				console.log(obj.placesId);
					
					var request = {
						placeId: obj.placesId
					};
					console.log(request);
					console.dir(request);

					service = new google.maps.places.PlacesService(map);
					service.getDetails(request, callback);

					function callback(place, status ) {
						if (status == google.maps.places.PlacesServiceStatus.OK) {
						console.dir(place);
						console.dir(obj);
							putOnPage(place , obj, true);
							$('.map-result:eq('+i+')').bind('click' , function () {
								mapUtils.openResultWindow($(this));
							});
						}
						else{
							console.log("*************Did not retrieve Places Information**************")
							console.log(status);
							console.dir(status);
						}
						i++;
						if ( i >= filtered.length) {
							generalUtils.cleanMapSideBar();
						}
					}
					
					
					
					
				}
				//If there is not a Places ID defined for the Business:
				else{
					putOnPage(obj , obj, false);
					$('.map-result:eq('+i+')').bind('click' , function () {
						mapUtils.openResultWindow($(this));
					});
					i++;
				}
            }

        },
        drawListUi : function (htmlContainer , templateString , filtered) {
            var i = 0;
            var cleanCategories = function() {
                var c = $('.category-list').text();
                s = c.replace(',' , '</li>');
                $('.cat').html('<li>'+s+'</li>');
            };
            htmlContainer.children().remove();

            for (var key in filtered) {
                var obj = filtered[key];
                var thisTemplateString = templateString.slice('');
                thisTemplateString = thisTemplateString.replace('{{result}}', parseInt(i) + 1);
                thisTemplateString = thisTemplateString.replace('{{name}}', obj.name);
                thisTemplateString = thisTemplateString.replace('{{PlaceName}}', obj.name);
                thisTemplateString = thisTemplateString.replace('{{Distance}}', obj.show);
                thisTemplateString = thisTemplateString.replace('{{Address1}}', obj.formattedAddress);
                thisTemplateString = thisTemplateString.replace('{{Address2}}', obj.city);
                thisTemplateString = thisTemplateString.replace('{{City}}', obj.city);
                thisTemplateString = thisTemplateString.replace('{{information}}', obj.information);
                thisTemplateString = thisTemplateString.replace('{{Phone}}', obj.phone);
                thisTemplateString = thisTemplateString.replace(/{{Website}}/g, obj.url);
                thisTemplateString = thisTemplateString.replace('{{categories}}', obj.categories);

                $(htmlContainer).append(thisTemplateString);
                i++;
            }
            // turn categories into list
            cleanCategories();
            generalUtils.listResultHandlers.init();

        }
    };

    var init = function () {
        generalUtils.dropDownHandlers();
        mapUtils.drawMap();
        mapUtils.checkForQuery();
        generalUtils.bindSearchBtn();
    };

    init();



});