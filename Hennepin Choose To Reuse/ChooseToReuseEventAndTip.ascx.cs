﻿namespace Hennepin.SCWeb.ChooseToReuse
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using Sitecore.Data.Items;
    using Sitecore.Web.UI.WebControls;
    using Sitecore.Collections;
    using System.Linq;
    using Sitecore.Data.Fields;

    public partial class ChooseToReuseEventAndTip : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item currentItem = Sitecore.Context.Item;
                Item rootItem = currentItem.Axes.SelectSingleItem("/sitecore/content/Choose to Reuse");

                Sitecore.Data.Fields.ImageField fldLargeImage = ((Sitecore.Data.Fields.ImageField)currentItem.Fields["Large Image"]);

                string bgImageUrl = "";
                litPictureBlock.Text = "<div class=\"picture-block\"></div>";

                if (fldLargeImage.MediaItem != null)
                {
                    bgImageUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(fldLargeImage.MediaItem);
                    litPictureBlock.Text = string.Format("<div class=\"picture-block\" style=\"background-image: url('{0}');\"></div>", bgImageUrl);
                }

                fldTitle.Item = currentItem;
                fldContent.Item = currentItem;

                //Prev and next buttons
                Item[] eventAndTipItems = rootItem.Axes.SelectItems("descendant::*[@@templateid='{EB0616E4-FF30-4D1E-9279-09230D315D25}' or @@templateid='{206A65E3-3E6D-4C07-976A-15181DD02474}']");

                if (eventAndTipItems.Length > 0)
                {
                    IEnumerable<Item> IEventItems = eventAndTipItems.OrderByDescending(i => i["Date"]);

                    List<Item> eventAndTipItemsList = eventAndTipItems.ToList<Item>();

                    int index = eventAndTipItemsList.IndexOf(currentItem);
                    int counter = 0;

                    foreach (Item curItm in eventAndTipItemsList)
                    {
                        if (curItm.ID == currentItem.ID)
                        {
                            break;
                        }
                        else
                        {
                            counter++;
                        }
                    }

                    int nextNewsIndex = Math.Min(eventAndTipItemsList.Count, counter + 1);
                    int prevNewsIndex = Math.Max(0, counter - 1);

                    if (nextNewsIndex == eventAndTipItemsList.Count)
                    {
                        nextNewsLink.Visible = false;
                    }
                    else
                    {
                        nextNewsLink.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(eventAndTipItemsList.ElementAt(nextNewsIndex));
                    }

                    if (counter == 0)
                    {
                        prevNewsLink.Visible = false;
                    }
                    else
                    {
                        prevNewsLink.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(eventAndTipItemsList.ElementAt(prevNewsIndex));
                    }
                }


            }
           catch (Exception ex)
            {
                bool exHandled = handleException(ex);
            }
        }

        Boolean handleException(Exception ex)
        {
            bool handled = false;
            try
            {
                handled = true;
            }
            catch (Exception e)
            {
                bool exHandled = handleException(ex);
            }

            return handled;
        }
        
    }
}