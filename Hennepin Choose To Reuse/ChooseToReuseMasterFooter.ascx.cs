﻿namespace Hennepin.SCWeb.ChooseToReuse
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using Sitecore.Data.Items;
    using Sitecore.Web.UI.WebControls;
    using Sitecore.Collections;
    using System.Linq;
    using Sitecore.Data.Fields;

    public partial class ChooseToReuseMasterFooter : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item currentItem = Sitecore.Context.Item;
                Item rootItem = currentItem.Axes.SelectSingleItem("/sitecore/content/Choose to Reuse");
                Item footerConfig = rootItem.Axes.SelectSingleItem("descendant::*[@@templateid='{F824F92E-ABF7-4C71-A02A-6EFA74D7679F}']");


                fldFooterLogo.Item = footerConfig;
                fldFooterCopyrightText.Item = footerConfig;
            }

            catch (Exception ex)
            {
                bool exHandled = handleException(ex);
            }
        }

        Boolean handleException(Exception ex)
        {
            bool handled = false;
            try
            {
                handled = true;
            }
            catch (Exception e)
            {
                bool exHandled = handleException(ex);
            }

            return handled;
        }
    }
}