﻿namespace Hennepin.SCWeb.ChooseToReuse
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using Sitecore.Data.Items;
    using Sitecore.Web.UI.WebControls;
    using Sitecore.Collections;
    using System.Linq;
    using Sitecore.Data.Fields;

    public partial class ChooseToReuseHomeBelowFold : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item currentItem = Sitecore.Context.Item;
                //ChildList childItems = currentItem.Children;

                Item[] eventItems = currentItem.Axes.SelectItems("descendant::*[@@templateid='{EB0616E4-FF30-4D1E-9279-09230D315D25}']");
                Item[] tipItems = currentItem.Axes.SelectItems("descendant::*[@@templateid='{206A65E3-3E6D-4C07-976A-15181DD02474}']");

                //Item[] businessItems = currentItem.Axes.SelectItems("descendant::*[@@templateid='{8677BB01-F25F-45E8-ABA3-B94DE925D1C3}' and @Featured Date > '201601010000000']");
                Item[] businessItems = Sitecore.Context.Database.SelectItems("fast://sitecore/content/#Choose to Reuse#/Businesses//*[@@templateid='{8677BB01-F25F-45E8-ABA3-B94DE925D1C3}' and @Featured Date > '201601010000000']");


                IEnumerable<Item> featuredBusinessList = businessItems.ToList().OrderByDescending(i => i["Featured Date"]);

                businessItems = featuredBusinessList.Take(1).ToArray();

                if (featuredBusinessList.Any())
                {
                    rptFeaturedBusiness.DataSource = businessItems;
                    rptFeaturedBusiness.DataBind();
                }

                //Could be refactored
                List<Item> eventItemsList = new List<Item>();

                foreach (Item eventItem in eventItems)
                {
                    CheckboxField checkboxField = eventItem.Fields["Display on Home Page"];
                    bool showHome = checkboxField.Checked;
                    if (showHome)
                    {
                        eventItemsList.Add(eventItem);
                    } 
                }

                if (eventItemsList.Count > 0)
                {
                    rptHomeEvents.DataSource = eventItemsList.OrderByDescending(i => i["Date"]).Take(6);
                    rptHomeEvents.DataBind();
                }

                List<Item> tipItemsList = new List<Item>();

                foreach (Item tipItem in tipItems)
                {
                    CheckboxField checkboxField = tipItem.Fields["Display on Home Page"];
                    bool showHome = checkboxField.Checked;
                    if (showHome)
                    {
                        tipItemsList.Add(tipItem);
                    }
                }

                if (tipItemsList.Count > 0)
                {
                    rptHomeTips.DataSource = tipItems.OrderByDescending(i => i["Date"]).Take(6);
                    rptHomeTips.DataBind();
                }
            }

            catch (Exception ex)
            {
                bool exHandled = handleException(ex);
            }
        }

        Boolean handleException(Exception ex)
        {
            bool handled = false;
            try
            {
                handled = true;
            }
            catch (Exception e)
            {
                bool exHandled = handleException(ex);
            }

            return handled;
        }
        protected void rptHomeEvents_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    Item nodeItm = (Item)e.Item.DataItem;
                    Text fldHeading = (Text)e.Item.FindControl("fldEventTitle");

                    Date fldEventDate = (Date)e.Item.FindControl("fldEventDate");

                    HyperLink itemLink = (HyperLink)e.Item.FindControl("itemLink");
                    itemLink.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(nodeItm);

                    Sitecore.Data.Fields.ImageField fldRollupImage = ((Sitecore.Data.Fields.ImageField)nodeItm.Fields["Rollup Image"]);
                    string bgImageUrl = "";
                    if (nodeItm.Fields["Rollup Image"].HasValue)
                    { 
                        bgImageUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(fldRollupImage.MediaItem);
                    }
                    

                    Literal litContainerDiv = (Literal)e.Item.FindControl("litContainerDiv");
                    litContainerDiv.Text = string.Format("<div class=\"box background\" style=\"background-image: url('{0}');\">", bgImageUrl);
                 
                    fldHeading.Item = nodeItm;
                    fldEventDate.Item = nodeItm;
                    
                }
                catch (Exception ex)
                {
                    bool exHandled = handleException(ex);
                }
            }
        }
        protected void rptHomeTips_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    Item nodeItm = (Item)e.Item.DataItem;
                    Text fldTipTitle = (Text)e.Item.FindControl("fldTipTitle");

                    HyperLink itemLink = (HyperLink)e.Item.FindControl("itemLink");
                    itemLink.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(nodeItm);

                    Sitecore.Data.Fields.ImageField fldRollupImage = ((Sitecore.Data.Fields.ImageField)nodeItm.Fields["Rollup Image"]);
                    string bgImageUrl = "";
                    if (nodeItm.Fields["Rollup Image"].HasValue)
                    {
                        bgImageUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(fldRollupImage.MediaItem);
                    }
                    Literal litContainerDiv = (Literal)e.Item.FindControl("litContainerDiv");
                    litContainerDiv.Text = string.Format("<div class=\"box background\" style=\"background-image: url('{0}');\">", bgImageUrl);

                    fldTipTitle.Item = nodeItm;
                }
                catch (Exception ex)
                {
                    bool exHandled = handleException(ex);
                }
            }
        }

        protected void rptFeaturedBusiness_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    Item nodeItm = (Item)e.Item.DataItem;
                    
                    Text fldBusinessName = (Text)e.Item.FindControl("fldBusinessName");
                    Text fldFeaturedSummary = (Text)e.Item.FindControl("fldFeaturedSummary");

                    Sitecore.Data.Fields.ImageField fldFeaturedImage = ((Sitecore.Data.Fields.ImageField)nodeItm.Fields["Featured Image"]);
                    string bgImageUrl = "";
                    if (nodeItm.Fields["Featured Image"].HasValue)
                    {
                        bgImageUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(fldFeaturedImage.MediaItem);
                    }

                    Literal litPictureBlock = (Literal)e.Item.FindControl("litPictureBlock");
                    litPictureBlock.Text = string.Format("<div class=\"picture-block\" style=\"background-image: url('{0}');\"></div>", bgImageUrl);

                    fldBusinessName.Item = nodeItm;
                    fldFeaturedSummary.Item = nodeItm;
                }
                catch (Exception ex)
                {
                    bool exHandled = handleException(ex);
                }
            }
        }

    }
}