namespace Hennepin.SCWeb.ChooseToReuse
{
    using System;
    using System.Web;
    using System.Web.UI;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using Sitecore.Data.Items;
    using Sitecore.Web.UI.WebControls;
    using Sitecore.Collections;
    using System.Linq;
    using Sitecore.Data.Fields;
    using System.Text;

    public partial class ChooseToReuseJSON : Page
    {
        int count = 0;

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                Item currentItem = Sitecore.Context.Item;
                Item rootItem = currentItem.Axes.SelectSingleItem("/sitecore/content/#Choose to Reuse#");
                //Item[] businessItems = rootItem.Axes.SelectItems("fast:/descendant::*[@@templateid='{8677BB01-F25F-45E8-ABA3-B94DE925D1C3}']");
                //Item[] businessItems = currentItem.Axes.SelectItems("fast:/sitecore/content/Choose to Reuse//*[@@templateid='{8677BB01-F25F-45E8-ABA3-B94DE925D1C3}']");

                Item[] businessItems = Sitecore.Context.Database.SelectItems("fast://sitecore/content/#Choose to Reuse#/Businesses//*[@@templateid='{8677BB01-F25F-45E8-ABA3-B94DE925D1C3}']");

                int numBusinesses = businessItems.Length;

                Item[] itemCategories = rootItem.Axes.SelectItems("descendant::*[@@templateid='{6A8A220F-7A43-462B-90C2-EA7D73D7DA2F}']");

                if (numBusinesses > 0)
                {
                    rptContacts.DataSource = businessItems;
                    count = businessItems.Length;
                    rptContacts.DataBind();
                }

                /*
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.Write(json);
                Response.End();
                */

            }

            catch (Exception ex)
            {
                bool exHandled = handleException(ex);
            }
        }

        Boolean handleException(Exception ex)
        {
            bool handled = false;
            try
            {
                handled = true;
            }
            catch (Exception e)
            {
                bool exHandled = handleException(ex);
            }

            return handled;
        }

        protected void rptContacts_PreRender(object sender, EventArgs e)
        {
            count = this.Items.Count;
        }

        protected void rptContacts_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    Item nodeItm = (Item)e.Item.DataItem;
                    int index = e.Item.ItemIndex;
                    //int count = rptContacts.Items.Count;

                    string cats = "\"\"";
 
                    Sitecore.Data.Fields.MultilistField fldCategories = nodeItm.Fields["Categories"];
                    if (fldCategories != null && fldCategories.Count > 0)
                    {
                        //Build string from GUID category items and Name fields.
                        cats = "\"";

                        int last = fldCategories.Items.Count() - 1;
                        int i = 0;
                        foreach (string id in fldCategories.Items)
                        {
                            string queryExpression = String.Format("/sitecore/content/Choose to Reuse/Configuration/Item Categories//*[@@ID='{0}']", id);
                            Item cat = Sitecore.Context.Item.Axes.SelectSingleItem(queryExpression);
                            try
                            {
                                string name = cat.Fields["Name"].ToString();
                                cats += name;
                            }
                            catch
                            {

                            }

                                if (i < last)
                                {
                                    cats += "|";
                                }
                                else
                                {
                                    cats += "\"";
                                }

                                i++;
                            
                            
                        }
                    }

                    //Assemble JSON Object
                    Literal litJSONObj = (Literal)e.Item.FindControl("litJSONObj");
                    StringBuilder sb = new StringBuilder();



                    //Address string from multiple fields
                    //Address Line 1
                    //Address Line 2
                    //City
                    //State
                    //Zip


                    StringBuilder sbAddress = new StringBuilder();
                    sbAddress.Append(nodeItm.Fields["Address Line 1"]);
                    sbAddress.AppendFormat(" {0}", nodeItm.Fields["Address Line 2"]);
                    sbAddress.AppendFormat(" {0}", nodeItm.Fields["City"]);
                    sbAddress.AppendFormat(", {0}", nodeItm.Fields["State"]);
                    sbAddress.AppendFormat(" {0}", nodeItm.Fields["Zip Code"]);

                    sb.Append("{");
                    sb.AppendFormat("\"id\": {0},", index);
                    //sb.AppendFormat("\"contactId\": {0},", 0);
                    sb.AppendFormat("\"name\": \"{0}\",", nodeItm.Fields["Business Name"].ToString());

                    sb.AppendFormat("\"phone\": \"{0}\",", nodeItm.Fields["Phone Number"].HasValue == false ? "" : nodeItm.Fields["Phone Number"].ToString());
                    sb.AppendFormat("\"url\": \"{0}\",", nodeItm.Fields["Website URL"].HasValue == false ? "" : nodeItm.Fields["Website URL"].ToString());
                    sb.AppendFormat("\"email\": \"{0}\",", nodeItm.Fields["Email Address"].HasValue == false ? "" : nodeItm.Fields["Email Address"].ToString());
                    sb.AppendFormat("\"formattedAddress\": \"{0}\",", sbAddress.ToString());
                    sb.AppendFormat("\"zipCode\": \"{0}\",", nodeItm.Fields["Zip Code"]);
                    sb.AppendFormat("\"city\": \"{0}\",", nodeItm.Fields["City"].ToString());


                    sb.AppendFormat("\"lat\": {0},", nodeItm.Fields["Latitude"].HasValue == false ? "0" : nodeItm.Fields["Latitude"].ToString());
                    sb.AppendFormat("\"lng\": {0},", nodeItm.Fields["Longitude"].HasValue == false ? "0" : nodeItm.Fields["Longitude"].ToString());
                    sb.AppendFormat("\"webOnly\": {0},", nodeItm.Fields["Web Only"].Value == "1" ? "\"true\"" : "\"false\"");
                    sb.AppendFormat("\"dropOff\": {0},", nodeItm.Fields["Drop Off"].Value == "1" ? "\"true\"" : "\"false\"");
                    sb.AppendFormat("\"pickUp\": {0},", nodeItm.Fields["Pick Up"].Value == "1" ? "\"true\"" : "\"false\"");
                    sb.AppendFormat("\"buysUsed\": {0},", nodeItm.Fields["Buys Used"].Value == "1" ? "\"true\"" : "\"false\"");
                    sb.AppendFormat("\"sellsUsed\": {0},", nodeItm.Fields["Sells Used"].Value == "1" ? "\"true\"" : "\"false\"");
                    sb.AppendFormat("\"repairs\": {0},", nodeItm.Fields["Repairs"].Value == "1" ? "\"true\"" : "\"false\"");
                    sb.AppendFormat("\"rents\": {0},", nodeItm.Fields["Rents"].Value == "1" ? "\"true\"" : "\"false\"");
                    sb.AppendFormat("\"other\": {0},", nodeItm.Fields["Other"].Value == "1" ? "\"true\"" : "\"false\"");
                    sb.AppendFormat("\"categories\": {0},", cats);
                    sb.AppendFormat("\"displayOnMap\": {0},", "\"true\"");
                    sb.AppendFormat("\"placesId\": \"{0}\"", nodeItm.Fields["Google Places ID"].ToString());
                    sb.Append("}");
                    if ((index + 1) < count) {
                        sb.Append(",");
                    }

                    litJSONObj.Text = sb.ToString();

                    /*
                {
                    "id": 452
                    , "contactId": 772
                    , "name": "2 Birds Vintage & Antiques"
                    , "phone": "(218) 969-1200"
                    , "url": "http://www.facebook.com/2BirdsAntiqueEmporium"
                    , "email": null
                    , "formattedAddress": "8509 Wyoming Ave N, Brooklyn Park, MN 55445"
                    , "city": "Brooklyn Park"
                    , "lat": 45.1090393
                    , "lng": -93.38451700000002
                    , "information": "An occasional shop featuring vintage and antique items, home & cabin decor, furniture, primitives and thrifty gifts."
                    , "webOnly": false
                    , "dropOff": false
                    , "pickUp": false
                    , "buysUsed": false
                    , "sellsUsed": false
                    , "repairs": false
                    , "rents": false
                    , "other": false
                    , "categories": "Antiques and Collectibles, Art and Home Decor"
                }
                ,
                     */


                }
                catch (Exception ex)
                {
                    bool exHandled = handleException(ex);
                }
            }
        }


    }
}
