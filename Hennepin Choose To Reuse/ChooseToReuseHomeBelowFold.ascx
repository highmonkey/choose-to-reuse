﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseToReuseHomeBelowFold.ascx.cs" Inherits="Hennepin.SCWeb.ChooseToReuse.ChooseToReuseHomeBelowFold" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="cp_event-tip-block">
    <section class="container">
        <div>
            <h3>Local events</h3>
        </div>
        

            <asp:Repeater ID="rptHomeEvents" runat="server" OnItemDataBound="rptHomeEvents_ItemDataBound">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <%# (Container.ItemIndex % 3 == 0) ? @"<div class='grid'>" : string.Empty %>
                    <div class="col-1-3">
                        <div class="grid-item">
                            <asp:HyperLink ID="itemLink" runat="server"> </asp:HyperLink>
                                <asp:Literal ID="litContainerDiv" runat="server" Text="<div class='box background'>"></asp:Literal>
                                    <!--background-image-->
                                    <div class="circle">
                                        <div class="circleText">
                                            <sc:Date ID="fldEventDate" runat="server" Format="MMM dd" Field="Date" />
                                        </div>
                                    </div>
                                    <div class="boxBottomBar">
                                        <p>
                                            <sc:Text ID="fldEventTitle" runat="server" Field="Rollup Title" />
                                        </p>
                                    </div>
                                </div><!--end Literal-->
                           
                        </div>
                     </div> 
                    <%# (Container.ItemIndex == 2 || (Container.ItemIndex + 1) % 3 == 0) ? @"</div>" : string.Empty %>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>

     
    </section>
</div>
<!--end cp_event-tip-block-->

<div class="cp_event-tip-block">
    <section class="container">
        <div>
            <h3>Tips and ideas</h3>
        </div>

        
            <asp:Repeater ID="rptHomeTips" runat="server" OnItemDataBound="rptHomeTips_ItemDataBound">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <%# (Container.ItemIndex % 3 == 0) ? @"<div class='grid'>" : string.Empty %>
                    <div class="col-1-3">
                        <div class="grid-item">
                            <asp:HyperLink ID="itemLink" runat="server"></asp:HyperLink>
                            <asp:Literal ID="litContainerDiv" runat="server" Text="<div class='box background'>"></asp:Literal>
								<div class="boxBottomBar">
									<p>
										<sc:Text ID="fldTipTitle" runat="server" Field="Rollup Title" />
									</p>
								</div>
							</div><!--end literal-->
						</div>
                    </div>
                    <%# (Container.ItemIndex == 2 || (Container.ItemIndex + 1) % 3 == 0) ? @"</div>" : string.Empty %>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
       
    </section>
</div>
<!--end cp_event-tip-block-->

 <div class="cp_picture-text-block">
            <section class="container">
                <div class="block">
                    <div class="inner-block">
                        <div class="grid">
                        <asp:Repeater ID="rptFeaturedBusiness" runat="server" OnItemDataBound="rptFeaturedBusiness_ItemDataBound">
                            <HeaderTemplate>
       
                            </HeaderTemplate>
                            <ItemTemplate>
                            <div class="col-1-3">
                                <asp:Literal ID="litPictureBlock" runat="server" Text=""></asp:Literal>
                            </div>
                            <div class="col-2-3">
                                <div class="text-block">
                                    <div class="twenty-six bold">FEATURED BUSINESS:</div>
                                    <h4 class="regular">
                                        <sc:Text ID="fldBusinessName" runat="server" Field="Business Name" />
                                    </h4>
                                    <p>
                                        <sc:Text ID="fldFeaturedSummary" runat="server" Field="Featured Summary" />
                                    </p>
                                    <a href="/choosetoreuse/featured">See other previously featured businesses</a>
                                </div>
                            </div>
                            </ItemTemplate>
                            <FooterTemplate>
                     
                            </FooterTemplate>
                        </asp:Repeater>
                         </div>
                      <!--end grid-->
                    </div>
                </div>
            </section>
        </div>
        <!--end cp_picture-text-block-->

<div class="cp_text-button-block">
    <section class="container">
        <div class="text-block">
            <h4>Help us make Choose to Reuse an even better resource for you.</h4>
        </div>
        <div class="button-block">
            <a href="#listbusiness">
                <div class="btn">List your business</div>
            </a>
            <a href="#addideaevent">
                <div class="btn">Add an idea or event</div>
            </a>
        </div>
    </section>
</div>
<!--end cp_text-button-block-->
