﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseToReuseHomeHero.ascx.cs" Inherits="Hennepin.SCWeb.ChooseToReuse.ChooseToReuseHomeHero" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="cp_index-hero-selection">
    <section class="container">

        <!--<form action="">-->
        <div class="menu-main">
            <h1>I would like to</h1>
            <!-- Desktop ONLY -->
            <div class="menu-bar services hidden-xs">
                <a data-value="sellsUsed" class="menu-selection active">
                    <span class="twenty-six">Sell</span>
                </a>
                <a data-value="donate" class="menu-selection">
                    <span class="twenty-six">Donate</span>
                </a>
                <a data-value="buysUsed" class="menu-selection">
                    <span class="twenty-six">Buy</span>
                </a>
                <a data-value="repairs" class="menu-selection">
                    <span class="twenty-six">Repair</span>
                </a>
                <a data-value="rent" class="menu-selection">
                    <span class="twenty-six">Rent</span>
                </a>
                <a data-value="share" class="menu-selection">
                    <span class="twenty-six">Share</span>
                </a>
            </div>
            <!-- Mobile ONLY -->
            <div class="menu-bar-mobile visible-xs">
                <div class="btn-group">
                    <button type="button" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <div class="semibold" data-bind="label">Sell</div>
                        <div class="toggle icon-caret-up"></div>
                    </button>
                    <ul class="services dropdown-menu close" role="menu">
                        <li>
                            <a href="#" data-value="sellsUsed" class="active">Sell</a>
                        </li>
                        <li>
                            <a href="#" data-value="donate">Donate</a>
                        </li>
                        <li>
                            <a href="#" data-value="buysUsed">Buy</a>
                        </li>
                        <li>
                            <a href="#" data-value="repairs">Repair</a>
                        </li>
                        <li>
                            <a href="#" data-value="rent">Rent</a>
                        </li>
                        <li>
                            <a href="#" data-value="share">Share</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--end menu-main-->
        <div class="location-form-container">
            <div class="location-dropdown">
                <select name="categories">

                <asp:Repeater ID="rptCategoryDropdownItems" runat="server" OnItemDataBound="rptCategoryDropdownItems_ItemDataBound">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litItem" runat="server" Text=""></asp:Literal>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
                </asp:Repeater>

                </select>
            </div>
            <div class="location-form">
                <div class="icon-magnifying-glass"></div>
                <input class="zip regular" type="text" placeholder="Zip code" name="zip" />
                <span class="hidden-xs">.</span>
            </div>
            <button class="btn twenty-six regular" id="baseSearch" value="search">Search</button>
        </div>
        <!--</form>-->
    </section>
</div>
<!--end cp_index-hero-selection-->

<div class="bottom-bar">
    <section class="container">
        <div class="bar-header-block">
            <h5>
                <sc:Text ID="fldBottomBarHeader" runat="server" Field="Bottom Bar Header" />
		        <span class="caret icon-caret-up"></span>
                <span class="caret icon-caret-down"></span>
            </h5>
        </div>
        <div class="bar-text-block collapse">
            <p><sc:Text ID="fldBottomBarText" runat="server" Field="Bottom Bar Text" /></p>
        </div>
    </section>
</div>
<!--end bottom-bar-->