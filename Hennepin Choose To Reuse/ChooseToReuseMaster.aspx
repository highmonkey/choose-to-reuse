﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" Inherits="Hennepin.SCWeb.ChooseToReuse.ChooseToReuseMaster" CodeBehind="ChooseToReuseMaster.aspx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7]>	<html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>		<html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>		<html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>		<html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <sc:Placeholder ID="phMetadata" runat="server" Key="phMetadata"></sc:Placeholder>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <script src="/choose-assets/js/modernizr.js"></script>

    <link href="/choose-assets/css/main.min.css" rel="stylesheet">
    <link href="/choose-assets/css/uniform._base.css" rel="stylesheet">

    <sc:VisitorIdentification runat="server" />
</head>

<body>

    <sc:Placeholder ID="phMasterHeader" runat="server" Key="phMasterHeader"></sc:Placeholder>

    <sc:PlaceHolder ID="phContent" runat="server" Key="phContent"></sc:PlaceHolder>

    <sc:Placeholder ID="phMasterFooter" runat="server" Key="phMasterFooter"></sc:Placeholder>

    <script src="/choose-assets/js/jQuery-1.12.min.js"></script>
    <script src="/choose-assets/js/jquery.uniform.js"></script>
    <script src="/choose-assets/js/ie-placeholder.js"></script>
    <script src="/choose-assets/js/main.js"></script>
    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyADf90Zmiiaj8GNZtur35RZ547P1LAULSs&libraries=places,visualization&sensor=false&extension=.js'></script>
    <script src="/choose-assets/js/custom-map.js"></script>
    <script id="locationTemplate" type="text/template">
	    <div class="map-result box">
		    <div class="row">
			    <div class="circle"></div>
			    <div class="title twenty semibold">{{name}}</div>
		    </div>
		    <div class="row">
			    <div class="directions"><a class="sixteen" target="_blank" href="{{googleMap}}">Directions</a></div>
			    <div class="website"><a class="sixteen" target="_blank" href="{{Website}}">Website</a></div>
		    </div>
		    <div class="row">
			    <div class="phone sixteen">{{Phone}}</div>
			    <!--<a href="tel:{{Phone}}" class="tel">Contact</a>-->
		    </div>
		    <div class="row">
			    <div class="hours sixteen">
				    <span class="twelve bold">HOURS</span>
				    <ul class="hours-list">
					    <li><span class="sixteen">{{hoursM}}</span></li>
					    <li><span class="sixteen">{{hoursT}}</span></li>
					    <li><span class="sixteen">{{hoursW}}</span></li>
					    <li><span class="sixteen">{{hoursTH}}</span></li>
					    <li><span class="sixteen">{{hoursF}}</span></li>
					    <li><span class="sixteen">{{hoursS}}</span></li>
					    <li><span class="sixteen">{{hoursSU}}</span></li>
				    </ul>
			    </div>
		    </div>
		    <div class="row">
			    <div class="address-title twelve bold">ADDRESS</div>
			    <div class="address-physical sixteen">{{Address}}</div>
		    </div>
		    <div class="rule"></div>
		    <div class="drawer">
			    <div class="row">
				    <div class="services-title twelve bold">SERVICES</div>
				    <ul class="services-list sixteen">
					    <li class="{{sellsUsed}}">Sells</li>
					    <li class="{{dropOff}}">Accepts donations - drop off</li>
					    <li class="{{pickup}}">Aceepts donations - pick up</li>
					    <li class="{{buysUsed}}">Buys</li>
					    <li class="{{repairs}}">Repairs</li>
					    <li class="{{rents}}">Rents</li>
					    <li class="{{share}}">Share</li>
				    </ul>
			    </div>
			    <div class="row">
				    <div class="description-title twelve bold">DESCRIPTION</div>
				    <div class="description-text">
					    <p class="sixteen">{{information}}</p>
				    </div>
			    </div>
		    </div>
	    </div><!--end box-->
    </script>
    <script id="locationTemplateNoPlacesID" type="text/template">
	    <div class="map-result box">
		    <div class="row">
			    <div class="circle"></div>
			    <div class="title twenty semibold">{{name}}</div>
		    </div>
		    <div class="row">
			    <div class="directions"><a class="sixteen" target="_blank" href="{{googleMap}}">Directions</a></div>
			    <div class="website"><a class="sixteen" target="_blank" href="{{Website}}">Website</a></div>
		    </div>
		    <div class="row">
			    <div class="phone sixteen">{{Phone}}</div>
			    <!--<a href="tel:{{Phone}}" class="tel">Contact</a>-->
		    </div>
		    <div class="row">
			    <div class="address-title twelve bold">ADDRESS</div>
			    <div class="address-physical sixteen">{{Address}}</div>
		    </div>
		    <div class="rule"></div>
		    <div class="drawer">
			    <div class="row">
				    <div class="services-title twelve bold">SERVICES</div>
				    <ul class="services-list sixteen">
					    <li class="{{sellsUsed}}">Sells</li>
					    <li class="{{dropOff}}">Accepts donations - drop off</li>
					    <li class="{{pickup}}">Aceepts donations - pick up</li>
					    <li class="{{buysUsed}}">Buys</li>
					    <li class="{{repairs}}">Repairs</li>
					    <li class="{{rents}}">Rents</li>
					    <li class="{{share}}">Share</li>
				    </ul>
			    </div>
			    <div class="row">
				    <div class="description-title twelve bold">DESCRIPTION</div>
				    <div class="description-text">
					    <p class="sixteen">{{information}}</p>
				    </div>
			    </div>
		    </div>
	    </div><!--end box-->
    </script>
    <script id="listTemplate" type="text/template">
	    <div class="list-result box">
		    <div class="title twenty semibold collapse-toggle">{{name}}</div>
		    <div class="circle collapse-toggle">
			    <div class="circleText">
				    <div class="icon-caret-up-skinny"></div>
				    <div class="icon-caret-down-skinny"></div>
			    </div>
		    </div>
		    <div class="col">
			    <div class="phone sixteen">{{Phone}}</div>
		    </div>
		    <div class="col">
			    <div class="website"><a class="sixteen" target="_blank" href="{{Website}}">Website</a></div>
		    </div>
		    <div class="drawer collapse">
			    <div class="col">
				    <div class="services-title twelve bold">SERVICES</div>
				    <ul class="services-list sixteen">
					    <li class="{{sellsUsed}}">Sells</li>
					    <li class="{{dropOff}}">Accepts donations - drop off</li>
					    <li class="{{pickup}}">Aceepts donations - pick up</li>
					    <li class="{{buysUsed}}">Buys</li>
					    <li class="{{repairs}}">Repairs</li>
					    <li class="{{rents}}">Rents</li>
					    <li class="{{share}}">Share</li>
				    </ul>
				    <div class="description-title twelve bold">DESCRIPTION</div>
				    <div class="description-text sixteen">
					    <div>{{information}}</div>
				    </div>
			    </div>
			    <div class="col">
				    <div class="services-title twelve bold">CATEGORIES</div>
				    <ul class="category-list sixteen">
					    {{categories}}
				    </ul>
			    </div>
		    </div>
	    </div>
    </script>
</body>
</html>
