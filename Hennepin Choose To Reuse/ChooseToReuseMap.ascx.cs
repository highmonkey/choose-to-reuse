﻿namespace Hennepin.SCWeb.ChooseToReuse
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using Sitecore.Data.Items;
    using Sitecore.Web.UI.WebControls;
    using Sitecore.Collections;
    using System.Linq;
    using Sitecore.Data.Fields;

    public partial class ChooseToReuseMap : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item currentItem = Sitecore.Context.Item;
                Item rootItem = currentItem.Axes.SelectSingleItem("/sitecore/content/Choose to Reuse");
                Item headerConfig = rootItem.Axes.SelectSingleItem("descendant::*[@@templateid='{9375D22E-FB20-43E5-A715-CE5898F3C7D1}']");

                Item[] itemCategories = rootItem.Axes.SelectItems("descendant::*[@@templateid='{6A8A220F-7A43-462B-90C2-EA7D73D7DA2F}']");

                if (itemCategories.Length > 0)
                {
                    rptCategoryDropdownItems.DataSource = itemCategories;
                    rptCategoryDropdownItems.DataBind();
                    rptCategoryDropdownItemsMobile.DataSource = itemCategories;
                    rptCategoryDropdownItemsMobile.DataBind();
                }

                //Text that appears below map. Dependant on dropdown selection.
                fldBeforeYouSellText.Item = currentItem;
                fldBeforeYouDonateText.Item = currentItem;
                fldBeforeYouBuyText.Item = currentItem;
                fldBeforeYouRepairText.Item = currentItem;
                fldBeforeYouRentText.Item = currentItem;
                fldBeforeYouShareText.Item = currentItem;
            }

            catch (Exception ex)
            {
                bool exHandled = handleException(ex);
            }
        }

        Boolean handleException(Exception ex)
        {
            bool handled = false;
            try
            {
                handled = true;
            }
            catch (Exception e)
            {
                bool exHandled = handleException(ex);
            }

            return handled;
        }

        protected void rptCategoryDropdownItems_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    Item nodeItm = (Item)e.Item.DataItem;
                    int index = e.Item.ItemIndex;

                    Literal litItem = (Literal)e.Item.FindControl("litItem");
                    litItem.Text = String.Format("<option value=\"{0}\" data-index=\"{1}\">{0}</option>", nodeItm.Fields["Name"].ToString(), index);
                }
                catch (Exception ex)
                {
                    bool exHandled = handleException(ex);
                }
            }
        }
        protected void rptCategoryDropdownItemsMobile_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    Item nodeItm = (Item)e.Item.DataItem;
                    int index = e.Item.ItemIndex;

                    Literal litItem = (Literal)e.Item.FindControl("litItem");
                    litItem.Text = String.Format("<option value=\"{0}\" data-index=\"{1}\">{0}</option>", nodeItm.Fields["Name"].ToString(), index);
                }
                catch (Exception ex)
                {
                    bool exHandled = handleException(ex);
                }
            }
        }
    }
}