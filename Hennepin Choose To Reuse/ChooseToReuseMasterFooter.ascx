﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseToReuseMasterFooter.ascx.cs" Inherits="Hennepin.SCWeb.ChooseToReuse.ChooseToReuseMasterFooter" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="cp_footer">
    <section class="container">
        <div class="logo-block">
            <a target="_blank" href="http://www.hennepin.us/">
                <sc:Image ID="fldFooterLogo" runat="server" Field="Footer Logo" CssClass="" />
            </a>
        </div>
        <div class="copyright-block">
            <div class="copyright-block-text">
                <p>
                    <sc:Text ID="fldFooterCopyrightText" runat="server" Field="Footer Copyright Text" />
                </p>
                <div class="anchor-copyright-block-text">
                    <a href="#privacy">Privacy</a>
                    <!-- an internal link that opens the contact box at the top of the page-->
                    <a class="contact-toggle">Contact</a>
                </div>
            </div>
        </div>
    </section>
</div>
<!--end cp_footer-->
