﻿namespace Hennepin.SCWeb.ChooseToReuse
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using Sitecore.Data.Items;
    using Sitecore.Web.UI.WebControls;
    using Sitecore.Collections;
    using System.Linq;
    using Sitecore.Data.Fields;

    public partial class ChooseToReuseBlueHeader : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item currentItem = Sitecore.Context.Item;
                Item rootItem = currentItem.Axes.SelectSingleItem("/sitecore/content/Choose to Reuse");
                Item headerConfig = rootItem.Axes.SelectSingleItem("descendant::*[@@templateid='{9375D22E-FB20-43E5-A715-CE5898F3C7D1}']");


                fldHeaderLogo.Item = headerConfig;
                fldContactInformation.Item = headerConfig;
            }

            catch (Exception ex)
            {
                bool exHandled = handleException(ex);
            }
        }

        Boolean handleException(Exception ex)
        {
            bool handled = false;
            try
            {
                handled = true;
            }
            catch (Exception e)
            {
                bool exHandled = handleException(ex);
            }

            return handled;
        }
    }
}