﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hennepin.SCWeb.ChooseToReuse {
    
    
    public partial class ChooseToReuseMap {
        
        /// <summary>
        /// rptCategoryDropdownItems control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater rptCategoryDropdownItems;
        
        /// <summary>
        /// rptCategoryDropdownItemsMobile control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater rptCategoryDropdownItemsMobile;
        
        /// <summary>
        /// fldBeforeYouSellText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Text fldBeforeYouSellText;
        
        /// <summary>
        /// fldBeforeYouDonateText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Text fldBeforeYouDonateText;
        
        /// <summary>
        /// fldBeforeYouBuyText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Text fldBeforeYouBuyText;
        
        /// <summary>
        /// fldBeforeYouRepairText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Text fldBeforeYouRepairText;
        
        /// <summary>
        /// fldBeforeYouRentText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Text fldBeforeYouRentText;
        
        /// <summary>
        /// fldBeforeYouShareText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Text fldBeforeYouShareText;
    }
}
