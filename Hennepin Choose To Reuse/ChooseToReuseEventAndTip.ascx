﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseToReuseEventAndTip.ascx.cs" Inherits="Hennepin.SCWeb.ChooseToReuse.ChooseToReuseEventAndTip" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<div class="cp_back-to-home-block">
    <section class="container">
        <a href="/reuse">Back to home</a>
    </section>
</div>

<div class="cp_event-tip-detail-block">
	<section class="container">
		<div class="grid">
			<div class="col-1-2 title">
				<h3>
                    <sc:Text ID="fldTitle" runat="server" Field="Title" />
				</h3>
			</div>
			<div class="col-1-2"></div>
		</div>
		<!--<div class="title">
			<h4>Title of event or tip information to be placed here</h4>
		</div>-->
		<div class="grid">
			<div class="col-1-2">
				<asp:Literal ID="litPictureBlock" runat="server" Text=""></asp:Literal>
			</div>
			<div class="col-1-2">
				<div class="text-block">
					<sc:Text ID="fldContent" runat="server" Field="Content" />
				</div>
			</div>
		</div><!--end grid-->
		<div class="previous-next-block">

            <asp:HyperLink runat="server" ID="prevNewsLink" rel="prev" CssClass="previous">
                <span class="meta-nav"></span>
                Previous event or tip
            </asp:HyperLink>			
            <asp:HyperLink runat="server" ID="nextNewsLink" rel="next" CssClass="next">
                Next event or tip
                <span class="meta-nav"></span>
            </asp:HyperLink>

		</div>
	</section>
</div>