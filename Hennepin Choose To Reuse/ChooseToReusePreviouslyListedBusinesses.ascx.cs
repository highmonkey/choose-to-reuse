﻿namespace Hennepin.SCWeb.ChooseToReuse
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using Sitecore.Data.Items;
    using Sitecore.Web.UI.WebControls;
    using Sitecore.Collections;
    using System.Linq;
    using Sitecore.Data.Fields;

    public partial class ChooseToReusePreviouslyListedBusinesses : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item currentItem = Sitecore.Context.Item;
                //ChildList childItems = currentItem.Children;
                Item rootItem = currentItem.Axes.SelectSingleItem("/sitecore/content/Choose to Reuse");


                Item[] businessItems = rootItem.Axes.SelectItems("descendant::*[@@templateid='{8677BB01-F25F-45E8-ABA3-B94DE925D1C3}']");

                IEnumerable<Item> featuredBusinessList = businessItems.ToList().OrderByDescending(i => i["Featured Date"]);

                businessItems = featuredBusinessList.Skip(1).Take(3).ToArray();

                if (featuredBusinessList.Any())
                {
                    rptFeaturedBusiness.DataSource = businessItems;
                    rptFeaturedBusiness.DataBind();
                }
            }
            catch (Exception ex)
            {
                bool exHandled = handleException(ex);
            }
        }

        Boolean handleException(Exception ex)
        {
            bool handled = false;
            try
            {
                handled = true;
            }
            catch (Exception e)
            {
                bool exHandled = handleException(ex);
            }

            return handled;
        }

        protected void rptFeaturedBusiness_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    Item nodeItm = (Item)e.Item.DataItem;

                    Text fldBusinessName = (Text)e.Item.FindControl("fldBusinessName");
                    Text fldFeaturedSummary = (Text)e.Item.FindControl("fldFeaturedSummary");
                    Literal litContainerDiv = (Literal)e.Item.FindControl("litContainerDiv");
                    litContainerDiv.Text = "<div class=\"box background\"></div>";

                    Sitecore.Data.Fields.ImageField fldFeaturedImage = ((Sitecore.Data.Fields.ImageField)nodeItm.Fields["Featured Image"]);
                    
                    string bgImageUrl = String.Empty;

                    if (fldFeaturedImage.MediaItem != null)
                    {
                        bgImageUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(fldFeaturedImage.MediaItem);
                        litContainerDiv.Text = string.Format("<div class=\"box background\" style=\"background-image: url('{0}');\"></div>", bgImageUrl);
                    }

                    fldBusinessName.Item = nodeItm;
                    fldFeaturedSummary.Item = nodeItm;
                }
                catch (Exception ex)
                {
                    bool exHandled = handleException(ex);
                }
            }
        }
    }
}