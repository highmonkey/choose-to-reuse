﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" Inherits="Hennepin.SCWeb.ChooseToReuse.ChooseToReuseJSON" CodeBehind="ChooseToReuseJSON.aspx.cs" ContentType="application/json; charset=utf-8"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
{"contacts": [<asp:Repeater ID="rptContacts" runat="server" OnItemDataBound="rptContacts_ItemDataBound" OnPreRender="rptContacts_PreRender">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="litJSONObj" runat="server" Text=""></asp:Literal>
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:Repeater>]}