﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseToReusePreviouslyListedBusinesses.ascx.cs" Inherits="Hennepin.SCWeb.ChooseToReuse.ChooseToReusePreviouslyListedBusinesses" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<div class="cp_back-to-home-block">
    <section class="container">
        <a href="/reuse">Back to home</a>
    </section>
</div>

<div class="cp_previously_featured_business">
    <section class="container">
        <h3>Previously Featured Businesses</h3>
        <div class="grid">
            <asp:Repeater ID="rptFeaturedBusiness" runat="server" OnItemDataBound="rptFeaturedBusiness_ItemDataBound">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="col-1-3">
                        <asp:Literal ID="litContainerDiv" runat="server" Text=""></asp:Literal>
                        <div>
                            <div class="twenty-six">
                                <sc:Text ID="fldBusinessName" runat="server" Field="Business Name" />
                            </div>
                            <p>
                                <sc:Text ID="fldFeaturedSummary" runat="server" Field="Featured Summary" />
                            </p>
                        </div>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <!--end grid-->
    </section>
</div>
