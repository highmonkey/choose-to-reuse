﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseToReuseBlueHeader.ascx.cs" Inherits="Hennepin.SCWeb.ChooseToReuse.ChooseToReuseBlueHeader" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="cp_contact-block">
    <section class="container">
        <h2>Contact Us
			    <div class="icon-x-close"></div>
        </h2>
        <!--<div class="icon-x-close"></div>-->
        <sc:Text ID="fldContactInformation" runat="server" Field="Contact Information" />
    </section>
</div>

<div class="cp_header">
	<section class="container">
		<div class="logo-block">
			<a href="/reuse">
				<sc:Image ID="fldHeaderLogo" runat="server" Field="Header Logo" CssClass="" />
			</a>
			<p class="mobile-display semibold">Supporting Hennepin County's community<br>to make sustainable choices in everyday living.</p>
		</div>
		<div class="menu-block">
			<div class="menu-block-mobile">
				<!-- an internal link that opens the contact box at the top of the page-->
				<a class="contact-toggle semibold">Contact</a>
				<a class="semibold" href="/reuse">Home</a>
			</div>
		</div>
	</section>
</div>
